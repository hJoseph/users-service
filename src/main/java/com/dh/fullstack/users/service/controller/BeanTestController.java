package com.dh.fullstack.users.service.controller;

import com.dh.fullstack.users.service.bean.Asus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@RestController
@RequestMapping("/beans")
@RequestScope
public class BeanTestController {

  private  Integer value = 1;

  /* Dependecy inyeccion por propiedad*/
    @Autowired
    private  Asus asus;

  /* Dependecy inyeccion por seeter
    public void setAsus(Asus asus) {
        this.asus = asus;
    }
    */

   /*
   Dependecy inyeccion por constructor
    public AccountController(Asus asus) {
        this.asus = asus;
    }

    */
    @RequestMapping (value = "/asus", method = RequestMethod.GET)
    public Asus readAsus(){
        value = value +1;
       asus.setName(asus.getName() + ": GET"+ ",Value: "+  value);
        return asus;
    }



  /*public  AccountController(){
      this.asus = new Asus(); No usar jamas
  }*/




}
