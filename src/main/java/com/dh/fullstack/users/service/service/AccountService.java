package com.dh.fullstack.users.service.service;

import com.dh.fullstack.users.service.input.AccountInput;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class AccountService {

    private AccountInput accountInput;

    @Autowired
    private AccountRepository accountRepository;

    public Account save() {
        return accountRepository.save(composeAccountIstance());

    }

    private Account composeAccountIstance() {
        Account instance = new Account();
        instance.setState(accountInput.getState());
        instance.setEmail(accountInput.getEmail());
        return  instance;
    }

    public void setAccountInput(AccountInput accountInput) {
        this.accountInput = accountInput;
    }
}
