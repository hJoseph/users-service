package com.dh.fullstack.users.service.config;

import com.dh.fullstack.users.service.bean.Asus;
import com.dh.fullstack.users.service.bean.Samsung;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Configuration
public class Config {

    @Bean
    @Scope("prototype")
    public Asus beanAsus() {
        Asus asus = new Asus();
        asus.setName("soy asusu");
        return asus;
    }

    @Bean
    @Scope("prototype")
    public Samsung beanSamsung() {
        Samsung samsung = new Samsung();
        samsung.setName("I am portatil the type samsung");
        return samsung;
    }
}
