package com.dh.fullstack.users.service.bean;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class Samsung {
    private  String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
