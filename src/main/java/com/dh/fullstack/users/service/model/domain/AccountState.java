package com.dh.fullstack.users.service.model.domain;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
