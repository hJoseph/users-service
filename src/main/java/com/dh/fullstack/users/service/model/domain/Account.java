package com.dh.fullstack.users.service.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Entity
@Table (name = "account_table",

        uniqueConstraints = {
                @UniqueConstraint(columnNames = {
                        "email"
                })}
)

@ApiModel (description = "All  detail  about account")

public class Account {
    @Id
    @ApiModelProperty(notes =  "the database generated Account ID")
    @Column(name = "accountid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ApiModelProperty(notes =  "the account email")
    @Column(name = "email", length =100,  nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "state", length =20,  nullable = false)
    private AccountState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountState getState() {
        return state;
    }

    public void setState(AccountState state) {
        this.state = state;
    }
}
