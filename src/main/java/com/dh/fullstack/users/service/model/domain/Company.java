package com.dh.fullstack.users.service.model.domain;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Entity
@Table(name = "Company_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "companyid",
                referencedColumnName = "userid")
})
// referencia el userid del padre extends
public class Company  extends User{

    @Column(name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
