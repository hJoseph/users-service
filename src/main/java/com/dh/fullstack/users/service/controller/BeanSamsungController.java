package com.dh.fullstack.users.service.controller;

import com.dh.fullstack.users.service.bean.Asus;
import com.dh.fullstack.users.service.bean.Samsung;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@RestController
@RequestMapping("/beans")
@RequestScope
public class BeanSamsungController {

  private  Integer value = 1;

    @Autowired
    private Samsung samsung;

    @RequestMapping (value = "/samsung", method = RequestMethod.GET)
    public Samsung readSamsung(){
        value = value +1;
        samsung.setName(samsung.getName() + ": GET"+ ",Value: "+  value);
        return samsung;
    }


}
