package com.dh.fullstack.users.service.model.repositories;

import com.dh.fullstack.users.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
