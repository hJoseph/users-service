package com.dh.fullstack.users.service.model.repositories;

import com.dh.fullstack.users.service.model.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface AccountRepository extends JpaRepository<Account,Long> {

}
