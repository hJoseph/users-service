package com.dh.fullstack.users.service.model.domain;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Entity
@Table(name = "Employee_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid",
                referencedColumnName = "userid")
})
// referencia el userid del padre extends
public class Employee extends User {

    @Column(name = "firstName", length = 50, nullable = false)
    private String firstName;
    @Column(name = "lastanme", length = 50, nullable = false)
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
